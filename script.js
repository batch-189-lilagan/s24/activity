let num = 2;
let getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);

// 

let address = [
    '14',
    'Balayan Street',
    'Quezon City'
];

const [houseNumber, street, city] = address;
console.log(`I live at #${houseNumber} ${street}, ${city}`);

// 

const animal = {
    name: 'Lolong',
    type: 'saltwater crocodile',
    weight: 1075,
    length: 20,
    height: 3
};

const { name, type, weight, length, height } = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${length} ft ${height} in.`);

// 

const numbers = [1,2,3,4,5];
numbers.forEach(eachNumber => {
    console.log(eachNumber);
});

// 

class Dog {
    constructor (inputName, inputAge, inputBreed) {
        this.name = inputName;
        this.age = inputAge;
        this.breed = inputBreed;
    }
};

let tala = new Dog ('Tala', 4, 'Shih Tzu');

console.log(tala);

